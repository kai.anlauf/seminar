numSets = 3;  % Number of sets

% Create the HDF5 filef
filename = 'dataTset1.h5';
file = H5F.create(filename);

% Create the dataset group

plist = 'H5P_DEFAULT';
group = H5G.create(file, '/dataset_group',plist,plist,plist);
%group_id = H5G.create(file, '/dataset_group');
%H5G.close(group_id);


for index = 1:numSets %run for amount of files needed
filename = 'dataTset1.h5';  %var needs to be there every loop. Keep this in.
%% init
    % Load EIDORS
    run startup.m
    
    %make meshes
    mesh_13 = mk_common_model('j2C', 16); % finer circular mesh
    mesh_16 = mk_common_model('j2C', 16);
    
    %create sim img objects
    sim_img_13 = mk_image(mesh_13,1);
    sim_img_16 = mk_image(mesh_16,1);
    
    %define and assign stim patterns
    stim =  mk_stim_patterns(16,1,'{ad}','{ad}',{'no_meas_current'},1);
    sim_img_13.fwd_model.stimulation = stim;
    
    stim2 = mk_stim_patterns(16,1,'{ad}','{ad}',{'meas_current'},1);
    sim_img_16.fwd_model.stimulation = stim2;

%% random vals // uniformly distributed

    R = 1; % radius of FEM model
    x0 = 0; % Center of the circle in the x direction.
    y0 = 0; % Center of the circle in the y direction.
    t = 2*pi*rand();
    r = R*rand();

    xr = x0 + r.*cos(t);
    yr = y0 + r.*sin(t);
    
    selectionRadius = rand(); % 0-1
    selectionScalar = rand(); % 0-1

 %% selection
    select_fcn =  @(x,y,z) (x-xr).^2+(y-yr).^2<(selectionRadius^2);


    sim_img_13.elem_data = selectionScalar + elem_select(sim_img_13.fwd_model,select_fcn);
    sim_img_16.elem_data = selectionScalar + elem_select(sim_img_16.fwd_model,select_fcn);

   
    diagVals = sprintf('Diag: Center = %f,%f , Radius = %f ,SelectionScalar = %f',xr,yr,selectionRadius,selectionScalar);
    disp (diagVals);

 %% 
    
    hmg_data_13=fwd_solve(sim_img_13);
    hmg_data_16=fwd_solve(sim_img_16);

    meas1 = hmg_data_13.meas;
    meas2 = hmg_data_16.meas;

%% map 208 vect to 256 vect, fill in with 0s

 % legacy
% remapIndex = '0011111111111110000111111111111110001111111111111100011111111111111000111111111111110001111111111111100011111111111111000111111111111110001111111111111100011111111111111000111111111111110001111111111111100011111111111111000111111111111110000111111111111100';
% zerosMap = find(remapIndex == '0'); 

zerosMap = [1 2 16 17 18 19 34 35 36 51 52 53 68 69 70 85 86 87 102 103 104 119 120 121 136 137 138 153 154 155 170 171 172 187 188 189 204 205 206 221 222 223 238 239 240 241 255 256]; 
newVec = zeros([256 1],'double');

remapTo = setdiff(1:length(newVec), zerosMap);
measRowVec = meas1.';
newVec(remapTo) = measRowVec;


%% map the vectors to Matrices

Mat1 = reshape(newVec, [16, 16]); %no_meas_current
Mat2 = reshape(meas2, [16, 16]);  %meas_current

    %% plot

     figure;
     show_fem(sim_img_13);
% 
%     figure;
%     show_fem(sim_img_16);

%% HDF5 STUFF

    % Create the matrix_13 dataset
    Matrix_13_data = Mat1;  
    Matrix_13_name = ['/dataset_group/Matrix_13_', num2str(index)];
    h5create(sprintf(filename), Matrix_13_name, size(Matrix_13_data));
    h5write(sprintf(filename), Matrix_13_name, Matrix_13_data);

    % Create the matrix_16 dataset
    Matrix_16_data = Mat2;  
    Matrix_16_name = ['/dataset_group/Matrix_16_', num2str(index)];
    h5create(sprintf(filename), Matrix_16_name, size(Matrix_16_data));
    h5write(sprintf(filename), Matrix_16_name, Matrix_16_data);

    count = sprintf('Complete: %d of %d', index, index); %numSets doesn't get stored past 1st run wtf
    disp(count);
clear;
end


% Close the HDF5 file
% H5F.close(filename);