%Setup Model

imdl = mk_common_model('e2c',16);
sim_img = mk_image(imdl,1);

stim_meas =  mk_stim_patterns(16,1,'{ad}','{ad}',{'meas_current'},1);
sim_img.fwd_model.stimulation = stim_meas;

sim_img_V = mk_image(imdl,1);
stim_V =  mk_stim_patterns(16,1,'{ad}','{ad}',{'no_meas_current'},1);
sim_img_V.fwd_model.stimulation = stim_V;

S = calc_jacobian( sim_img );

hom_data=fwd_solve(sim_img);
hom_data_V=fwd_solve(sim_img_V);

select_fcn =  @(x,y,z) (x).^2+(y).^2<0.9^2;
sim_img.elem_data = 1 + elem_select(sim_img.fwd_model,select_fcn);

inh_data=fwd_solve(sim_img);

show_fem(sim_img);

sim_img_V.elem_data = 1 + elem_select(sim_img_V.fwd_model,select_fcn);
inh_data_V = fwd_solve(sim_img_V);

figure;
ydata = inh_data.meas;
xdata = (1:length(ydata));
plot(xdata, ydata)
ylabel("Voltage/V");
xlabel("Measurement");

hold on;

% map 208 vect to 256 vect, fill in with 0s
 
zerosMap = [1 2 16 17 18 19 34 35 36 51 52 53 68 69 70 85 86 87 102 103 104 119 120 121 136 137 138 153 154 155 170 171 172 187 188 189 204 205 206 221 222 223 238 239 240 241 255 256]; 
newVec = zeros([256 1],'double');

remapTo = setdiff(1:length(newVec), zerosMap);
measRowVec = (inh_data_V.meas - hom_data_V.meas);
newVec(remapTo) = measRowVec;

% map the vectors to Matrices

V_jk = reshape(newVec, [16, 16]);

% Method Harrach

A_j_kl = zeros(16, 16, 16);

b_j_k = zeros(16, 1, 16);

A = zeros(16, 16, 16);

b = zeros(16, 1, 16);

S_b = zeros(16, 16);

for i = 1:1:1600

    S_i = reshape(S(:,i), [16, 16]);
    S_b = S_b + S_i;

end

S_b_plus = pinv(S_b);

for j = 1:1:16
    for k = 1:1:16
        for l = 1:1:16
           
           if j-1 == 0
                j_1 = 16;
           else
    	        j_1 = j-1;
           end

           if j+1 == 17
                j__1 = 1;
           else
                j__1 = j+1;   
           end

            A_j_kl(k,l,j) = eq(k, j_1)*eq(l,j) + eq(k, j__1)*eq(l, j__1) - eq(k,j)*eq(l,j) - eq(k,j)*eq(l, j__1);
    
        end
    end
end

for j = 1:1:16
    for k = 1:1:16

        switch abs(k-j)

            case 1
                b_j_k(k,1,j) = 0;

            case 0
                sum = 0;
                for l = 1:1:16
                    if abs(l-j) > 1
                        sum = sum + V_jk(j,l);
                    end
                end
                b_j_k(k,1,j) = -sum;

            otherwise
                if abs(k-j) > 1
                    b_j_k(k,1,j) = V_jk(j,k);
                end
        end
    end
end

sum = 0;
for j = 1:1:16
    sum = sum + ( transpose(A_j_kl(:,:,j)) * S_b_plus * A_j_kl(:,:,j) );
end
A = -sum;

sum = 0;
for j = 1:1:16
    sum = sum + ( transpose(A_j_kl(:,:,j)) * S_b_plus * b_j_k(:,1,j) );
end
b = -sum;

w = - ( A \ b );

V_geom = A_j_kl(:,:,1) * w + b_j_k(:,1,1);

for j = 2:1:16

    V_geom = [V_geom ,A_j_kl(:,:,j) * w + b_j_k(:,1,j)];

end

% fill in measurement matrix with interpolated values
 
zerosMap = [1 2 16 17 18 19 34 35 36 51 52 53 68 69 70 85 86 87 102 103 104 119 120 121 136 137 138 153 154 155 170 171 172 187 188 189 204 205 206 221 222 223 238 239 240 241 255 256]; 
newVec = reshape(V_geom, [256, 1]) + hom_data.meas;

remapTo = setdiff(1:length(newVec), zerosMap);
newVec(remapTo) = inh_data_V.meas;

% map the vectors to Matrices

V_end = reshape(newVec, [16, 16]);

inh_matrix = reshape(inh_data.meas, [16, 16]);

% end result

end_data = reshape(V_end, [256, 1]);
x_end_data = (1:length(end_data));
plot(x_end_data, end_data)
ylabel("Voltage/V");
xlabel("Measurement");
legend({'Accurate measurements','Interpolated data'});

figure;
ydata_diff = (inh_data.meas - hom_data.meas);
xdata_diff = (1:length(ydata_diff));
subplot(2,1,1);
plot(xdata_diff, ydata_diff)
ylabel("Voltage/V");
xlabel("Measurement");

hold on;

interp_data = reshape(V_geom, [256, 1]);
x_interp_data = (1:length(interp_data));
subplot(2,1,1);
plot(x_interp_data, interp_data)
legend({'Accurate measurements','Interpolated data'});

% Root Mean Squared Error

RMSE = sqrt(mean((ydata - end_data).^2))

% Relative interpolation error in the Frobenius norm

test = (ydata - end_data);

norm_geom = norm((ydata - end_data),"fro");

norm_ref = norm(ydata,"fro");

err = norm_geom / norm_ref






