%Setup Model

imdl = mk_common_model('e2c',16);

sim_img = mk_image(imdl,1);
sim_img_ref = mk_image(imdl,1);

stim =  mk_stim_patterns(16,1,'{ad}','{ad}',{'no_meas_current'},1);
stim_ref =  mk_stim_patterns(16,1,'{ad}','{ad}',{'meas_current'},1);

sim_img.fwd_model.stimulation = stim;
sim_img_ref.fwd_model.stimulation = stim_ref;

select_fcn =  @(x,y,z) (x-0.1).^2+(y-0.2).^2<0.2^2;
sim_img.elem_data = 1 + elem_select(sim_img.fwd_model,select_fcn);
sim_img_ref.elem_data = 1 + elem_select(sim_img_ref.fwd_model,select_fcn);

show_fem(sim_img);

inh_data=fwd_solve(sim_img);
inh_data_ref=fwd_solve(sim_img_ref);
ref = inh_data_ref.meas;

figure;
ydata = inh_data.meas;
xdata = (1:length(ydata))';
subplot(2,1,1);
plot(xdata, ydata)
ylabel("Voltage/V");
xlabel("Measurement");

%Interpolation Spline

interpol_func = @(x) spline(xdata, ydata, x);

xi1 = linspace(0, 207, 256);
yi1 = interpol_func(xi1)';

subplot(2,1,2);
xdatai1 = 1:length(yi1);
plot(xdatai1, yi1)
ylabel("Voltage/V");
xlabel("Measurement");

%Interpolation Grid interp

xi2 = linspace(0, 207, 256);
yi2 = interp1(xdata,ydata,xi2,'cubic')';

figure;
ydata = inh_data.meas;
xdata = (1:length(ydata))';
subplot(2,1,1);
plot(xdata, ydata)
ylabel("Voltage/V");
xlabel("Measurement");

subplot(2,1,2);
xdatai2 = 1:length(yi2);
plot(xdatai2, yi2);
ylabel("Voltage/V");
xlabel("Measurement");

% map 208 vect to 256 vect, fill in with 0s

zerosMap = [1 2 16 17 18 19 34 35 36 51 52 53 68 69 70 85 86 87 102 103 104 119 120 121 136 137 138 153 154 155 170 171 172 187 188 189 204 205 206 221 222 223 238 239 240 241 255 256]; 
newVec = zeros([256 1],'double');
newVeci = yi1;

remapTo = setdiff(1:length(newVeci), zerosMap);
measRowVec = ydata.';
newVeci(remapTo) = measRowVec;
newVec(remapTo) = measRowVec;

figure;
xdata = (1:length(ref))';
subplot(2,1,1);
plot(xdata, ref)
ylabel("Voltage/V");
xlabel("Measurement");

subplot(2,1,2);
plot(xdatai1, newVeci)
ylabel("Voltage/V");
xlabel("Measurement");

% map 208 vect to 256 vect, fill in with 0s

newVeci2 = yi2;

remapTo = setdiff(1:length(newVeci2), zerosMap);
measRowVec = ydata.';
newVeci2(remapTo) = measRowVec;

figure;
xdata = (1:length(ref))';
plot(xdata, ref)
ylabel("Voltage/V");
xlabel("Measurement");

hold on;

plot(xdatai1, newVeci2)
ylabel("Voltage/V");
xlabel("Measurement");
legend({'Accurate measurements','Interpolated data'});

% map the vectors to Matrices

Mat = reshape(newVec, [16, 16])';
Mati = reshape(newVeci, [16, 16])';

% Errors for Interpolation Spline

% Root Mean Squared Error

RMSE = sqrt(mean((ref - newVeci).^2))

% Relative interpolation error in the Frobenius norm

norm_geom = norm((ref-newVeci),"fro");

norm_ref = norm(ref,"fro");

err = norm_geom / norm_ref

% Errors for Interpolation Grid interp

% Root Mean Squared Error

RMSE2 = sqrt(mean((ref - newVeci2).^2))

% Relative interpolation error in the Frobenius norm

norm_geom2 = norm((ref-newVeci2),"fro");

norm_ref2 = norm(ref,"fro");

err2 = norm_geom2 / norm_ref2






