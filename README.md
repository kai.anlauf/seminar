# seminar
Seminar Medizintechnik

Requirements for the following files
•	Matlab R2021b or later
•	EIDORS for Matlab and running the startup file

interpol_cubic.m
In the Matlab file “interpol_cubic.m” there is the implementation of a cubic interpolation of EIT data. After running the file there will be five different graphs shown. The first figure is a visual representation of the EIDORS model from which the EIT measurement values are taken. In the next two windows there will be two graphs in each window for comparison. The upper one in both is showing the 208 elements long measurement matrix without all the values that involve the current-driven electrodes. Below that in figure 2 there is the cubic interpolation result by using spline interpolation and in figure 3 there is the cubic interpolation result when using 1-D data interpolation in Matlab.
Figure 4 and 5 both show two graphs as well for comparing them. In both the upper graphs the results of a perfect measurement with perfect measurements, even for those involving current-driven electrodes, resulting in a 256 elements long measurement matrix. In figure 4 the lower graph shows the 208 elements long measurement matrix with the interpolated values added from the spline interpolation, while figure 5 shows it with the interpolated values from the 1-D data interpolation added. These interpolated values are added at the appropriate locations where the measurements from current driven electrodes would be.

interpol_geom_V2_3.m
Harrach`s geometric interpolation method is implemented in the Matlab file “interpol_geom_V2_3.m”. After running the file there will be three different graphs shown. The first figure is a visual representation of the EIDORS model from which the EIT measurement values are taken. Figure 2 then shows the interpolation results by using the method as described in Harrach´s paper “Interpolation of missing electrode data in electrical impedance tomography”. Figure 3 shows the geometrically interpolated matrix compared to the ideal difference measurement matrix.
In the command window there is also the Root Mean Squared Error and the Relative interpolation error in the Frobenius norm, when comparing the interpolated data with the perfectly measured data, being shown.

Requirements for the following files
•	MATLAB R2022b or later. Some required libraries and functions are not available in older versions of matlab.
•	EIDORS for Matlab and running the startup file
•	Python 3.10 or later
•	Conda, jupyter, h5py, tensorflow, cuda etc. are required to smoothly run the machine learning tasks. 
o	Make sure to use conda to set up your environment, libraries and notebooks. 
o	Tensorflow (specially with GPU acceleration) has certain specific package version requirements on certain platforms. Refer the documentation and use conda to install into a managed venv, to avoid messing with the rest of your system.
•	Google Colab can be used, avoiding the need to set up a machine learning environment and expensive hardware on your part. This reduces the chances of Freezing PCs and VRAM overflows, but it may be harder to manage files in and out of the system.
•	HDFview may simplify debugging your hdf porting code by being able to directly view and read the HDF large data files that you may need.

zaks_4.m and zaks_5.m
Creates the datasets used for training the ‘hypermatrix’ in an HDF5 file called “dataT.h5”. For this, in Matlab a large amount of randomized EIT models is created and the measurement data is acquired by forward solving using EIDORS. The data is split up into the measurement data from all electrodes including current-driven ones and the smaller set of data measured without using the current-driven electrodes. If there is an error, clearing the workspace and deleting any existing file with the name “dataT.h5” may help. The difference between these two files is that zaks_4.m outputs vectors and zaks_5.m mapped matrices.

The 5 ipynb notebooks:
1-vectormapping.ipynb 
This notebook deals with using sequential dense neural networks with various activation functions to see how well they map a 208x1 vector to a 256x1 vector. Experiment and see if the variables and activation functions lead to decent results. Note that the dense nature means they work best with vectors, not matrices.
2-vector_mapped_masked_customloss.ipynb
This one has our custom masked vectors and custom loss functions applied to the above approach. Look through the code, there’s surely a lot to be optimised here.
3-einsum_basic.ipynb
This one has our basic Einsum trainable hypermatrix being used to figure out a solution. 
4-einsum_customloss_pp.ipynb
My personal recommendation, this one is our Einsum approach, however with a custom loss function with tuneable weights, and an almost-complete post-processing system to drastically reduce the unnecessary errors. This works because we already have the zeros map and the original data to replace the noisy predicted data and can use some theoretical knowledge to improve the output data. 
5-autoencoder TEST, BUGGED.ipynb
Uses the theoretically best VAE (variational autoencoder) approach to this problem. Ran into VRAM issues and only worked marginally well on colab, so proceed with caution.
Note that when it comes to the notebooks, once you have calculated a decent approximation to W, you can simply output it as a .npy file. You can then use that in any kind of python script, cast it into an array to use in another program, even in real-time. However, make sure you also script out the post processing code block, as it improves the output data by a lot. 
An alternative would be to simply import your incomplete data matrix into the notebook and apply the Einsum right there, but its practicality is left as an exercise to the reader. Note that the zeros_map is available in the notebook and one may use it to map any 208x1 vector to the right 16x16 matrix.

The three HDF5 datasets
HDF (hierarchical data formats) provide a rigid way to port over large sets of data from Matlab to TensorFlow in Python for our uses. They provide a way to have groups of data, and custom named data paths so we can signify the relationship between the x and y datasets in the files when accessing the data from the files. 
Make sure to have the h5py library for python and h5 (included in newer versions of Matlab) library for Matlab for smooth operation. 
•	data.h5 100 datasets of 2 vectors, for fast operation
•	data2.h5 1000 datasets of 2 vectors, for somewhat better fitting
•	dataM.h5 1000 datasets of 2 matrices, already fitted, for inputting data with spatial relationships

The two csv datasets
•	To use for quick checks
